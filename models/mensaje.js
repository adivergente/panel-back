  'use strict'
// Cargamos el módulo de mongoose
var mongoose =  require('mongoose');
// Usaremos los esquemas
var Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos
var MensajeSchema = Schema({
  id: Number,
  id_compra: Number,
  id_cliente: String,
  mensaje:String,
  file:String,
  id_atiende:String
});
// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('Mensaje', MensajeSchema);
