'use strict'
// Cargamos el módulo de mongoose
var mongoose =  require('mongoose');
// Usaremos los esquemas
var Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos
var UsuariosSchema = Schema({
  id: String,
  datos_personales:
  {
    nombres: String,
    apellidos: String,
    username: String,
    email: String,
    password: { type: String, select: false },
    telefono: String
  },
  domicilio: {
    type: Object,
    default: {
      num: '',
      calle: '',
      colonia: '',
      // direccion: '',
      localidad: '',
      municipio: '',
      estado: '',
      pais: 'México',
      codigo_postal: '',
      referencias: ''
    }
  },
  rol: {
    type: Array,
    default: []
  },
  status: {
    type: String,
    default: 'Activo'
  },
  reset_password: {
    token: '',
    expires_at: { type: Date }
  },
  email_verified_at: {
    type: Date,
    default: null
  }
});

// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('Usuarios', UsuariosSchema);
