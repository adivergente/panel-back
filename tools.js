module.exports = {
  validateLogin: function (params) {
    if (params.hasOwnProperty("usuario") && params.hasOwnProperty("password")) {
      return (params.usuario.length > 3 && params.password.length > 5) ? true : false;
    } else {
      return false;
    }
  }
}
