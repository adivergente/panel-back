const express = require('express')
const router = express.Router()
const Roles = require('../models/roles')
const Usuarios = require('../models/usuarios')
const { isAuthenticated } = require('../middlewares/authMiddleware')

router.post('/buscar', function(req, res, next) {

  console.log("datos rol: ",req.body.roles)
  const query = { 'nombre': { $in: req.body.roles } }
  console.log("query:", query)

  Roles.find(query, (err, response) => {
    if (err) res.status(404).send({ success: false, message: 'Ningun registro identificado' })
    return res.json({ success: true, message: '', data: filteredLinks(response) })
  })
})

router.post('/nuevo', function(req, res, next) {

  var datos = req.body;
  console.log("datos rol: ",datos)
  var query = {
    //'ruta': "/Users/macbookair/Documents/refacesys/replace-sys/admin-backend/app/public/imagenes_promociones/" + req.files.img1.name,
    'nombre': datos.nombre,
    'links': JSON.stringify(datos.links)
  }
  console.log("query:", query)
  //return res.status(200).send({success: true,message: 'regreso'});
  //metodo para buscar el producto
  Roles.create(query)
  .then((rawResponse) =>{
    if(!rawResponse){
      return res.status(404).send({success: false,message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      return res.json({
          "success": true,
          "data": rawResponse,
          "message": "Encontrado"
        })
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send({success: false,message: 'Error en la consulta'});
  });
});

// Get all
router.get('/', function(req, res, next) {
  Roles.find({}, (err, response) => {
    if (err) return res.status(404).send({ success: false, message: err })
    return res.json({ success: true, message: '', data: response })
  })
})

// Crear nuevo rol
router.post('/', isAuthenticated, (req, res, next) => {
  // Validar que el nombre sea unico
  Roles.find({ 'nombre': req.body.nombre })
    .then( response => {
      if (response.length) return res.send({ success: false, message: 'Ya existe un rol con este nombre', data: { type: 'name' } })

      const rol = new Roles(req.body)
      rol.save(err => {
        if (err) return res.send({ success: false, message: err, data: {} })
        return res.send({ success: true, message: 'Rol agregado correctamente', data: {} })
      })
    })
    .catch( err => {
      console.log(err)
      return res.status(500).send('Error en la peticion');
    })
})

// Actualizar rol
router.post('/update', isAuthenticated, (req, res, next) => {
  const query = {
    $set: {
      'nombre': req.body.nombre,
      'links': req.body.links
    }
  }
  
  Roles.findOneAndUpdate({ '_id': req.body._id }, query, (err, rol) => {
    if(!rol) return res.status(404).send({ success: false, message: 'Error al actualizar', data: null })
    // si se modifica el nombre del rol, actualizar usuarios que tengan agregado este rol
    if (rol.nombre !== req.body.nombre) {
      Usuarios.updateMany({ 'rol': rol.nombre }, { $set: { 'rol.$': req.body.nombre } }, (err, users) => {
        if (err) res.json({ success:true, message:'Rol actualizado (fallo actualizar usuarios)', data: rol })
        return res.json({ success:true, message: `Rol actualizado. ${users.nModified} usuarios actualizados`, data: rol })
      })  
    } else {
      // Usamos else para evitar que se ejecute al terminar if
      return res.json({ success:true, message: 'Rol actualizado', data: rol })
    }
  })
})

// Eliminar rol
router.post('/remove', isAuthenticated, (req, res, next) => {
  console.log('eliminar rol: ', req.body._id)
  Roles.findByIdAndRemove(req.body._id, (err, rol) => {
    if (err) return res.send({ success: false, message: err, data: null })
    // console.log('rol eliminado:', rol)
    Usuarios.updateMany({ }, { $pull: { rol: rol.nombre } }, (err, usuarios) => {
      if (err) res.send({ success: true, message: 'Rol eliminado (usuarios no actualizados)', data: null })
      // console.log('cant usuarios:', usuarios.nModified)
      return res.send({ success: true, message: `Rol eliminado, ${usuarios.nModified} usuarios actualizados`, data: null })
    })
  })
})

function filteredLinks (roles) {
  const arr = []
  roles.forEach(rol => {
    rol.links.forEach(link => {
      arr.push(link)
    })
  })
  // linea que no entiendo :(
  return [...new Map(arr.map(item => [item.text, item])).values()]
}

module.exports = router;
