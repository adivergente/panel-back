var express = require('express');
var router = express.Router();
var sinonimos = require('../models/sinonimos');

router.get('/list-sinonimos', function(req, res, next) {
    sinonimos.find({ }, function (err, sinonimos){
        if(err){
            return res.status(500).send('Error en la peticion');
        }
        if(!sinonimos){
            console.log(sinonimos)
            return res.status(404).send({message: 'Ningun registro identificado'});
        }
        console.log(sinonimos)
        return res.json(sinonimos);
    });
});

router.post('/edit', function(req, res, next) {
    var data = req.body;
   
    var query1 = {
        '_id': data._id
    }
    var query2 = {
        'sinonimos':data.sinonimos
    }
    //metodo para buscar el producto
    sinonimos.updateOne(query1, query2)
      .then((sinonimos) => {
        console.log('actualizado')
        console.log(sinonimos)
        var result ={success:true,data:sinonimos, message:"Datos actualizados"}
        return res.json(result);
      })
      .catch((err) => {
          console.log(err)
        return res.status(500).send({success:false, data:null, message:'Error en la peticion'});
      });
     
  });

  router.post('/alta', function(req, res, next) {
    var data = req.body;
   
    var query1 = {
        'palabra': data.palabra,
        'sinonimos':data.sinonimo
    }
    
    //metodo para buscar el producto
    sinonimos.create(query1)
      .then((sinonimos) => {
        console.log('subida')
        console.log(sinonimos)
        var result ={success:true,data:sinonimos, message:"Datos subidos"}
        return res.json(result);
      })
      .catch((err) => {
          console.log(err)
        return res.status(500).send({success:false, data:null, message:'Error en la peticion'});
      });
     
  });

module.exports = router;