const express = require('express')
const router = express.Router()
// const shajs = require('sha.js')
const Usuario = require('../models/usuarios')
// const Rol = require('../models/roles')
const { isAuthenticated } = require('../middlewares/authMiddleware')

/* GET users listing. */
router.get('/', isAuthenticated,  (req, res, next) => {
	Usuario.find({}, (err, usuarios) => {
    if (err) return res.send({ success: false, message: err, data: null })
    if (!usuarios.length) return res.send({ success: false, message: 'No data', data: null })
    return res.send({ success: true, message: '', data: usuarios })
  })
})

// router.post('/update', isAuthenticated, (req, res, next) => {

//   const data = req.body;
//   const query = {
//     $set: {
//       'datos_personales.nombres':          data.nombres,
//       'datos_personales.apellidos':        data.apellidos,
//       'datos_personales.telefono':         data.telefono,
//       'datos_personales.username':         data.username,
//       'domicilio.direccion':               data.direccion,
//       'domicilio.pais':                    data.pais,
//       'domicilio.estado':                  data.estado,
//       'domicilio.municipio':               data.municipio,
//       'domicilio.localidad':               data.localidad,
//       'domicilio.codigo_postal':           data.cp,
//       'domicilio.referencias':             data.referencias,
//       'rol': 															 data.rol
//     }
//   }
//   Usuario.findOneAndUpdate({ '_id': data._id }, query, { new:true }, (err, user) => {
//     if(!user){
//       return res.status(404).send({ success: false, message: 'Error al actualizar', data: [] });
//     }
//     return res.json({ success:true, message:'Datos actualizados', data: user })
//   });
// })

router.post('/change-status/:id', isAuthenticated, (req, res, next) => {
	const query = {
		$set: {
			'status': req.body.status
		}
	}
	Usuario.findOneAndUpdate({ '_id': req.params.id }, query, (err, user) => {
    if(!user){
      return res.status(404).send({ success: false, message: 'Error al actualizar', data: [] });
    }
    return res.json({ success:true, message:'Datos actualizados', data: user })
  });
})

module.exports = router
