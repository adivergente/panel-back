const express = require('express')
const router = express.Router()
const shajs = require('sha.js')
const Usuario = require('../models/usuarios')
const Rol = require('../models/roles')
const { isAuthenticated } = require('../middlewares/authMiddleware')

/* GET users listing. */
router.get('/', isAuthenticated,  (req, res, next) => {
	// Conseguir lista de roles dados de alta
	Rol.find({}, (err, roles) => {
		if (err) return res.send({ success: false, message: err, data: null })

		// Crear array de roles
		const rolesArr = roles.map(data => data.nombre )

		// Recorrer roles y generar query '$or', si contiene alguno de esos roles lo seleccionamos
		const or = []
		rolesArr.forEach(rol => {
			or.push({ rol })
		})
		const query = { $or: or	}

		Usuario.find(query, (err, usuarios) => {
	  	if (err) return res.send({ success: false, message: err, data: null })
	  	if (!usuarios.length) return res.send({ success: false, message: 'No data', data: null })
	  	return res.send({ success: true, message: '', data: usuarios })
	  })
	})
})
/* GET auth user info */
router.get('/me', isAuthenticated, (req, res, next) => {
  Usuario.findById(req.userId, (err, usuario) => {
    if (err) return res.send({ success: false, message: err, data: null })
      return res.send({ success: true, message: '', data: usuario })
  })
})
/* POST add user */
router.post('/', isAuthenticated, (req, res, next) => {
  const data = req.body;
  const pass = shajs('sha256').update(data.password).digest('hex')
  const user = {
    'nombres':          data.nombres,
    'apellidos':        data.apellidos,
    'username':         data.username,
    'email':            data.email.toLowerCase(),
    'password':         pass,
    'telefono':         data.telefono
  }
  const domicilio = {
  	'calle':       	    data.calle,
    'num':              data.num,
    'colonia':          data.colonia,
    'estado':          	data.estado,
    'municipio':       	data.municipio,
    'localidad':       	data.localidad,
    'codigo_postal':   	data.cp,
    'referencias':     	data.referencias,
    'pais': 						data.pais || 'México'
  }
  const query = {
    'id': 							data.email,
    'datos_personales': user,
    'domicilio': 				domicilio,
    'rol': 							data.rol
  }
  
  Usuario.find({ 'datos_personales.email': user.email })
  .then((rawResponse) =>{

    if (rawResponse.length) return res.send({ success: false, message: 'Lo sentimos, esta cuenta de correo ya fue registrada', data: 'email'})

    const newUser = new Usuario(query)
  	newUser.save(err => {
  		if (err) return res.send({ success: false, message: err, data: null })
			return res.send({ success:true, message: 'Nuevo usuario creado', data: null})
  	})
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});
/* POST Update user */
router.post('/update', isAuthenticated, (req, res, next) => {

  const data = req.body
  const query = {
    $set: {
      'datos_personales.nombres':          data.nombres,
      'datos_personales.email':            data.email,
      'datos_personales.apellidos':        data.apellidos,
      'datos_personales.telefono':         data.telefono,
      'datos_personales.username':         data.username,
      'domicilio.calle':                   data.calle,
      'domicilio.num':                     data.num,
      'domicilio.colonia':                 data.colonia,
      'domicilio.pais':                    data.pais,
      'domicilio.estado':                  data.estado,
      'domicilio.municipio':               data.municipio,
      'domicilio.localidad':               data.localidad,
      'domicilio.codigo_postal':           data.cp,
      'domicilio.referencias':             data.referencias,
      'rol': 															 data.rol
    }
  }

  Usuario.findOne({ '_id': data._id }, (err, us) => {
    if (!us) res.send({ success: false, message: 'Error consultado datos de usuario', data: [] })

    // Si no se va a ctualizar email
    if( us.datos_personales.email === data.email ) {
      Usuario.findOneAndUpdate({ '_id': data._id }, query, { new:true }, (err, user) => {
        if(!user) return res.send({ success: false, message: 'Error al actualizar', data: [] })
        return res.json({ success:true, message:'Datos actualizados', data: user })
      })
    } else {
      // Buscar si no existe un usuario registrado con ese nuevo email
      Usuario.findOne({ 'datos_personales.email': data.email }, (err, usuario) => {
        if(err) return res.send({ success: false, message: 'Error consultado datos de usuario', data: [] })

        // Si no existe se actualizan datos
        if( !usuario ) {
          Usuario.findOneAndUpdate({ '_id': data._id }, query, { new:true }, (err, user) => {
            if(!user) return res.send({ success: false, message: 'Error al actualizar', data: [] })
            return res.json({ success:true, message:'Datos actualizados', data: user })
          })
        } else {
          // Si ya existe se regresa error
          return res.send({ success: false, message: 'Esta cuenta de email ya existe', data: [] })
        }
      })
    }
  })
})
/* POST change status */
router.post('/change-status/:id', isAuthenticated, (req, res, next) => {
	const query = {
		$set: {
			'status': req.body.status
		}
	}
	Usuario.findOneAndUpdate({ '_id': req.params.id }, query, (err, user) => {
    if(!user){
      return res.status(404).send({ success: false, message: 'Error al actualizar', data: [] });
    }
    return res.json({ success:true, message:'Datos actualizados', data: user })
  });
})
/* POST change user password */
router.post('/change-password/:id', isAuthenticated, (req, res, next) => {
  const password = shajs('sha256').update(req.body.password).digest('hex')
  const query = {
    $set: {
      'datos_personales.password': password
    }
  }
  Usuario.findOneAndUpdate({ '_id': req.params.id }, query, (err, user) => {
    if(!user){
      return res.status(404).send({ success: false, message: 'Error al actualizar', data: [] });
    }
    return res.json({ success:true, message:`Actualizada contraseña para el usuario ${user.datos_personales.nombres} ${user.datos_personales.apellidos}`, data: null })
  });
})

module.exports = router
