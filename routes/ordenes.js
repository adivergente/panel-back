const express = require('express')
const router = express.Router()
const Orden = require('../models/ordenes')
const { isAuthenticated } = require('../middlewares/authMiddleware')

// Obtener todas las compras
router.get('/', isAuthenticated, (req, res, next) => {
	console.log('id', req.userId)
  // Orden.find({}, (err, ordenes) => {
  //   if (err) return res.send({ success: false, message: '', data: err })
  //   return res.send({ success: true, message: '', data: ordenes })
  // })
  Orden.aggregate([
		{
			$lookup:{
        from: "usuarios",
        localField:"userId",
        foreignField:"_id", 
        as: "usuario"
      }
		},
		{
			$lookup:{
        from: "usuarios",
        localField:"atiende",
        foreignField:"_id", 
        as: "user_atiende"
      }
		},
		{ $sort: { created_at: -1 } }
	], (err, ordenes) => {
		if (err) return res.send({ success: false, message: err, data: null })

		// console.log('ordenes', ordenes)
		return res.send({ success: true, message: '', data: ordenes })
	})
})

// Cambiar status de orden
router.post('/change-status/:id', isAuthenticated, (req, res, next) => {
  const query = {
    $set: {
      'status': req.body.status,
      'atiende': req.userId
    }
  }
  console.log(query)
  Orden.findOneAndUpdate({ '_id': req.params.id }, query, (err, order) => {
    if(!order){
      return res.status(404).send({ success: false, message: 'Error al actualizar', data: [] });
    }
    return res.json({ success:true, message:`Actualizada status de orden ${order.folio}`, data: null })
  })
})

// Subir guia de rastreo
router.post('/guia/:id', isAuthenticated, (req, res, next) => {
  const query = {
    $set: {
      'paqueteria.tracking_id': req.body.tracking,
      'paqueteria.nombre': req.body.paqueteria,
      'status': 'enviado'
    }
  }
  console.log(query)
  Orden.findOneAndUpdate({ '_id': req.params.id }, query, (err, order) => {
    if(!order){
      return res.status(404).send({ success: false, message: 'Error al actualizar', data: [] });
    }
    return res.json({ success:true, message:`Actualizada guia de orden ${order.folio}`, data: null })
  })
});


module.exports = router
