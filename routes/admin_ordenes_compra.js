var express = require('express');
var router = express.Router();
var compras_libre = require('../models/compras_libre');
var carrito_compras = require('../models/carrito-compras');
var facturas = require('../models/facturas');

/* GET all products */
router.get('/list-compras', function(req, res, next) {
  compras_libre.find({ }, function (err, compras_libre){
      if(err){
        return res.status(500).send('Error en la peticion');
      }
      if(!compras_libre){
        console.log(compras_libre)
        return res.status(404).send({message: 'Ningun registro identificado'});
      }
    return res.json(compras_libre);
  });
});

//Obtener datos de carritos por compra realizada

/* GET product by intern code */
router.get('/compras-data/:code', function(req, res, next) {
  var code = req.params.code;
  carrito_compras.find({ 'id_compras': code }, function (err, carrito_compras){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!carrito_compras)
        return res.status(404).send({message: 'Ningun registro identificado'});
    //console.log(compras_libre)
    return res.send(carrito_compras);
  });
});

router.post('/compras-data/', function(req, res, next) {
  console.log('datos: ',req.body)
  var id_compras = req.body.id_compras;
  var id_usuario = req.body.id_usuario
  var query = {$and: [
    { 'id_compras': id_compras },
    { 'id_usuario': id_usuario }
  ]}
  console.log('query: ',query)
  carrito_compras.find(query, function (err, carrito_compras){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!carrito_compras)
        return res.status(404).send({message: 'Ningun registro identificado'});
    //console.log(compras_libre)
    return res.send(carrito_compras);
  });
});

//Actualizar status de la compra

router.post('/compra-status', function(req, res, next) {
  var data = req.body;
//  var new_stock = 0;
  console.log(data)
  var id_compra = data[0].value;
  var estatus = data[1].value;
  var atiende = data[2].value;
  var cliente = data[3].value;
  console.log(id_compra)
  console.log(estatus)
  console.log(atiende)
  console.log(cliente)
  var query1 = {"_id": id_compra }

  var query2 = {
    'estado': estatus ,
    'atiende': atiende }
  
      //metodo que cambia el status
  console.log('query1: ',query1)
  console.log('query2: ',query2)

  /*compras_libre.findOneAndUpdate(id_compra, function (err, facturas){
    console.log('entro: ',facturas)
    if(err){
      console.log('error: ',err)
      return res.status(500).send('Error en la peticion');
    }
    if(!facturas){
      console.log('datos: ',facturas)
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log("facturas: ")
      console.log(facturas)
      return res.json(facturas);
    }
    
  });*/


  //compras_libre.updateOne
  //findOneAndUpdate
  compras_libre.updateOne(query1, query2)
    .then((result) => {
       console.log('update')
       console.log(result)
       return res.json({
        "success": true,
        "message": 'Estuatus actualizado',
        "data": result
      });
      })
      .catch((err) => {
        console.log('error: ',err)
        return res.status(500).send({
          "success": false,
          "message": 'Error al modificar'
        });
       
      });
    
});

router.post('/guia', function(req, res, next) {
  var data = req.body;
//  var new_stock = 0;
  console.log(data)
  var id_compra = data[0].value;
  var estatus = data[1].value;
  var atiende = data[2].value;
  var cliente = data[3].value;
  console.log(id_compra)
  console.log(estatus)
  console.log(atiende)
  console.log(cliente)
  var query1 = {"_id": id_compra }

  var query2 = {
    'estado': estatus ,
    'atiende': atiende }
  
      //metodo que cambia el status
  console.log('query1: ',query1)
  console.log('query2: ',query2) 

  compras_libre.updateOne(query1, query2)
    .then((result) => {
       console.log('update')
       console.log(result)
       return res.json({
        "success": true,
        "message": 'Estuatus actualizado',
        "data": result
      });
      })
      .catch((err) => {
        console.log('error: ',err)
        return res.status(500).send({
          "success": false,
          "message": 'Error al modificar'
        });
       
      });
    
});

//Actualizar referencia de Oxxo
router.post('/actualizar-referencia/:ref', function(req, res, next) {
  var num_ref = req.params.ref;
  console.log(num_ref);
//  var num_ref = req.params.referencia;
  console.log('correcto')
  //metodo que cambia el status
  facturas.updateOne({'numero_referencia': num_ref })
    .then((facturas) => {
        console.log('update')
        console.log(facturas)
        return res.json(facturas);
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
});

//Obtener dato de referencia de Oxxo
router.get('/obtener-referencia', function(req, res, next) {
  facturas.find({ }, function (err, facturas){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!facturas)
        return res.status(404).send({message: 'Ningun registro identificado'});
    console.log("facturas: ")
    console.log(facturas)
    return res.json(facturas);
  });
});

//Obtener compras por comprador

router.get('/mejores-compradores', function(req, res, next) {
  compras_libre.aggregate([
          {
            $group : {
              _id :  "$datos_personales.email" ,
              total_compra: { $sum : "$total_venta", },
              nombre: { $push: "$datos_personales.nombre_completo" } ,
              count : { $sum : 1 }
            }
           }
         ], function (err, compras_libre){
             if(err)
               return res.status(500).send('Error en la peticion');
             if(!compras_libre)
               return res.status(404).send({message: 'Ningun registro identificado'});
           console.log(compras_libre)
           return res.json(compras_libre);
         }).sort({total_compra:1}).limit(10);
});

router.get('/mejores-compradores2', function(req, res, next) {
  compras_libre.aggregate([
          {
            $group : {
              _id :  "$datos_personales.email" ,
              total_compra: { $sum : "$total_venta", },
              nombre: { $push: "$datos_personales.nombre_completo" } ,
              count : { $sum : 1 }
            }
           }
         ], function (err, compras_libre){
             if(err)
               return res.status(500).send('Error en la peticion');
             if(!compras_libre)
               return res.status(404).send({message: 'Ningun registro identificado'});
           //console.log(compras_libre)
           return res.json(compras_libre);
         }).sort({total_compra:-1}).limit(10);
});


//Obtener productos más vendidos

router.get('/productos-mas-vendidos', function(req, res, next) {
  carrito_compras.aggregate([
          {
            $group : {
              _id :  "$clave_productos" ,
              nombre: { $push : "$nombre" },
              cantidades: { $sum : "$cantidades" },
              count : { $sum : 1 }
            }
           }
         ], function (err, carrito_compras){
             if(err)
               return res.status(500).send('Error en la peticion');
             if(!carrito_compras)
               return res.status(404).send({message: 'Ningun registro identificado'});
           //console.log(compras_libre)
           return res.json(carrito_compras);
         }).sort({cantidades:1}).limit(10);
         //.limit(15).sort('field -test');
});
router.get('/productos-mas-vendidos2', function(req, res, next) {
  carrito_compras.aggregate([
          {
            $group : {
              _id :  "$clave_productos" ,
              nombre: { $push : "$nombre" },
              cantidades: { $sum : "$cantidades" },
              count : { $sum : 1 }
            }
           }
         ], function (err, carrito_compras){
             if(err)
               return res.status(500).send('Error en la peticion');
             if(!carrito_compras)
               return res.status(404).send({message: 'Ningun registro identificado'});
           //console.log(compras_libre)
           return res.json(carrito_compras);
         }).sort({cantidades:-1}).limit(10);
         //.limit(15).sort('field -test');
});



router.get('/categorias-mas-vendidas', function(req, res, next) {
  console.log('entro a categorias')
  carrito_compras.aggregate([
          {
            $lookup:{
              from: "productos",
              localField:"clave_productos",
              foreignField:"clave_interna", 
              as: "productos"
            }
          },
          {$match: {
              "id_compras": {$ne:""},
              "productos": {$ne:""}
              }
          },
          {$group : 
              { 
                  _id : "$productos.categoria",
                  cant : 
                      { $sum : 1 }
                  
              }
          } 
         ], function (err, carrito_compras){
             if(err)
               return res.status(500).send('Error en la peticion');
             if(!carrito_compras)
               return res.status(404).send({message: 'Ningun registro identificado'});
           console.log(carrito_compras)
           return res.json(carrito_compras);
         }).sort({cant:-1}).limit(10);
         //.limit(15).sort('field -test');
});

router.get('/categorias-mas-vendidas', function(req, res, next) {
  console.log('entro a categorias')
  compras_libre.aggregate([
        {
          $group : {
            _id :  "$datos_personales.email" ,
            total_compra: { $sum : "$total_venta", },
            nombre: { $push: "$datos_personales.nombre_completo" } ,
            count : { $sum : 1 }
          }
        }], function (err, carrito_compras){
             if(err)
               return res.status(500).send('Error en la peticion');
             if(!carrito_compras)
               return res.status(404).send({message: 'Ningun registro identificado'});
           console.log(carrito_compras)
           return res.json(carrito_compras);
         }).sort({cant:-1}).limit(10);
         //.limit(15).sort('field -test');
});


module.exports = router;
