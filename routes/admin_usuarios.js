var express = require('express');
var router = express.Router();
var usuario = require('../models/usuarios');
var shajs = require('sha.js')
var jwt = require('jsonwebtoken')

/* GET all users */
var tools = {
  keyToken: "c,3Y>sS;8}5[ubec",
  validateLogin: function (params) {
    if (params.hasOwnProperty("usuario") && params.hasOwnProperty("password")) {
      return (params.usuario.length > 3 && params.password.length > 5) ? true : false;
    } else {
      return false;
    }
  }
}

router.post('/nuevo', function(req, res, next) {
  var specific_data = req.body;
  console.log('Datos de usuario a actualizar')
  console.log(specific_data)
  //var id_user = specific_data[3].value;
  //console.log('Id de usuario')
  //console.log(id_user)
  var pass = shajs('sha256').update(specific_data.password).digest('hex')
  var query = {
    'id': "0",
    'datos_personales':
    {
      'nombres':  specific_data.nombres,
      'apellidos':  specific_data.apellidos,
      'username':         "",
      'email':            specific_data.email,
      'password':         pass,
      'fecha_nacimiento': "",
      'telefono':         ""
    },
    'domicilio':
    {
      'num_interior':     "",
      'num_exterior':     "",
      'calle':            "",
      'colonia':          "",
      'localidad':        "",
      'municipio':        "",
      'estado':           "",
      'pais':             "",
      'codigo_postal':    "",
      'referencias':      ""
    },
    'rol': specific_data.rol,
    'status':             "Activo"
    
  }
  //console.log("update de usuarios paso")
  //metodo para buscar el usuario
  usuario.create(query)
  .then((rawResponse) =>{
    if(!rawResponse){
      console.log("no encontro nada")
      return res.status(404).send({success:false,message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      return res.status(200).send({success:true,message: 'Registrado'});
      //metodo que cambia el stock
      
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send({success:false,message:'Error en la peticion'});
  });
});



router.get('/all-users', function(req, res, next) {
  usuario.find({ }, function (err, usuario){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!usuario)
        return res.status(404).send({message: 'Ningun registro identificado'});
    //console.log(usuario)
    return res.json(usuario);
  });
});

router.get('/all-users-admin', function(req, res, next) {
  usuario.find({"id":"0"}, function (err, usuario){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!usuario)
        return res.status(404).send({message: 'Ningun registro identificado'});
    //console.log(usuario)
    return res.json(usuario);
  });
});


router.get('/all-users/:id', function(req, res, next) {
  var id_user = req.params.id;
  usuario.findOne({ id: id_user }, function (err, usuario){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!usuario)
        return res.status(404).send({message: 'Ningun registro identificado'});
    console.log(usuario)
    return res.send(usuario);
  });
});


/* GET user by id */
router.post('/login', function(request, response, next) {
  const params = request.body;
    let valid = tools.validateLogin(params)    
    var pass = shajs('sha256').update(params.password).digest('hex')
    query = {$and: [{ "datos_personales.email": params.usuario },
                    // { "id": "0"},
                    { "datos_personales.password": pass}]
            }
    /*query = {
      'email': params.usuario,
      'id' : "0",
      'password': pass
    }*/
    console.log(query)
    if (valid) {
      usuario.find(query)
      .then((rawResponse) =>{
          console.log('encontro: ',rawResponse)
          if(!rawResponse.length || !rawResponse[0].rol.length){
            console.log('no entro')
            response.status(401).json({
              "success": false,
              "error": "Credenciales no válidas"
            });
          }else{
            console.log('entro')
            var datos = {
              'usuario':rawResponse[0].datos_personales.nombres,
              'username':rawResponse[0].datos_personales.username,
              '_id':rawResponse[0]._id,
              'id':rawResponse[0].datos_personales.email,
              'rol':rawResponse[0].rol,
            }
            console.log('datos:',datos)
            jwt.sign(datos, tools.keyToken, { expiresIn: '8h' }, (err, token) => {
              if(err){
                console.log('error token:',err)
              }else{
                console.log('token: ',token)
                response.status(200).json({
                  "success": true,
                  "token": token,
                  "data": rawResponse[0]
                })
              }
            })
          }
        }).catch(function (error) {
          response.status(500).json({ success: false, error: error })
        })
    } else {
      response.status(500).json({ success: false, error: error })
    }
});

/* Update user */
router.post('/update-user', function(req, res, next) {
  var specific_data = req.body;
  console.log('Datos de usuario a actualizar')
  console.log(specific_data)
  var id_user = specific_data[3].value;
  console.log('Id de usuario')
  console.log(id_user)
  var query = {
    $set: {
      'datos_personales.nombres':          specific_data[0].value,
      'datos_personales.apellidos':        specific_data[1].value,
      'datos_personales.username':         specific_data[2].value,
      'datos_personales.email':            specific_data[3].value,
      'datos_personales.fecha_nacimiento': specific_data[4].value,
      'datos_personales.telefono':         specific_data[5].value,
      'domicilio.num_interior':     specific_data[7].value,
      'domicilio.num_exterior':     specific_data[6].value,
      'domicilio.calle':            specific_data[8].value,
      'domicilio.colonia':          specific_data[9].value,
      'domicilio.localidad':        specific_data[10].value,
      'domicilio.municipio':        specific_data[11].value,
      'domicilio.estado':           specific_data[12].value,
      'domicilio.pais':             specific_data[13].value,
      'domicilio.codigo_postal':    specific_data[14].value,
      'domicilio.referencias':      specific_data[15].value
    }
  }
  console.log("update de usuarios paso")
  //metodo para buscar el usuario
  usuario.find({'id': id_user})
  .then((rawResponse) =>{
    if(!rawResponse){
      console.log("no encontro nada")
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      //metodo que cambia el stock
      usuario.update({'id': id_user}, query)
      .then((usuario) => {
        console.log('update')
        console.log(usuario)
        var result = {'success':true}
        console.log(result)
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

/* Update user */
router.post('/update-admin', function(req, res, next) {
  var specific_data = req.body;
  console.log('Datos de usuario a actualizar')
  console.log(specific_data)
  var query = {
    $set: {
      'datos_personales.nombres' :         specific_data[1].value,
      'datos_personales.apellidos':        specific_data[2].value,
      'datos_personales.username':         specific_data[3].value,
      'datos_personales.email':            specific_data[4].value,
      'datos_personales.fecha_nacimiento': specific_data[5].value,
      'datos_personales.telefono':         specific_data[6].value,
        'domicilio.num_interior':     specific_data[7].value,
        'domicilio.num_exterior':     specific_data[8].value,
        'domicilio.calle':            specific_data[9].value,
        'domicilio.colonia':          specific_data[10].value,
        'domicilio.localidad':        specific_data[11].value,
        'domicilio.municipio':        specific_data[12].value,
        'domicilio.estado':           specific_data[13].value,
        'domicilio.pais':             specific_data[14].value,
        'domicilio.codigo_postal':    specific_data[15].value
    }

  }
  console.log("update de usuarios paso")
  //metodo para buscar el usuario
  usuario.find({'id': '0'})
  .then((rawResponse) =>{
    if(!rawResponse){
      console.log("no encontro nada")
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      //metodo que cambia los valores del admin
      usuario.update({'id': '0'}, query)
      .then((usuario) => {
        console.log('update')
        console.log(usuario)
        var result = {'success':true}
        console.log(result)
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

//get current pass
router.post('/get-admin-pass', function(req, res, next) {
  console.log('llego aca')
  var specific_data = req.body;
  console.log(specific_data)
  var new_pass = specific_data[0].value;
  console.log(new_pass);
  var definit_pass = shajs('sha256').update(new_pass).digest('hex');

  usuario.findOne({ id : '0' })
    .then((rawResponse) =>{
      if(!rawResponse){
        console.log("no encontro nada")
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        console.log('correcto')
        console.log(rawResponse.datos_personales.password)

        var result = {'success':false};

        if ( rawResponse.datos_personales.password == definit_pass )
          var result = {'success':true}

        console.log(result)
        return res.json(result);
        //metodo que cambia los valores del admin
/*        usuario.update({'id': '0'}, query)
        .then((usuario) => {
          console.log('update')
          console.log(usuario)
          var result = {'success':true}
          console.log(result)
          return res.json(result);
        })
        .catch((err) => {
          return res.status(500).send('Error en la peticion');
        });
        */
      }
    })
    .catch((err) => {
      console.log(err)
      return res.status(500).send('Error en la peticion');
    });
});

//update Password
router.post('/update-password/:id', function(req, res, next) {
  var specific_data = req.params.id;
  var definit_pass = shajs('sha256').update(specific_data).digest('hex');
  console.log('Datos de usuario a actualizar')
  console.log(definit_pass)
  var query = {
    $set: {
      'datos_personales.password': definit_pass
    }

  }
  console.log("update de usuarios paso")
  //metodo para buscar el usuario
  usuario.find({'id': '0'})
  .then((rawResponse) =>{
    if(!rawResponse){
      console.log("no encontro nada")
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      //metodo que cambia los valores del admin
      usuario.update({'id': '0'}, query)
      .then((usuario) => {
        console.log('update')
        console.log(usuario)
        var result = {'success':true}
        console.log(result)
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

//create new usuario
router.post('/new-user', function(req, res, next) {
  var specific_data = req.body;
  var pass = shajs('sha256').update(specific_data[3].value).digest('hex')
  var query = {
    'id': specific_data[2].value,
    'datos_personales':
    {
      'nombre_completo':  specific_data[0].value,
      'username':         specific_data[1].value,
      'email':            specific_data[2].value,
      'password':         pass,
      'fecha_nacimiento': specific_data[4].value,
      'telefono':         specific_data[5].value
    },
    'domicilio':
    {
      'num_interior':     specific_data[6].value,
      'num_exterior':     specific_data[7].value,
      'calle':            specific_data[8].value,
      'colonia':          specific_data[9].value,
      'localidad':        specific_data[10].value,
      'municipio':        specific_data[11].value,
      'estado':           specific_data[12].value,
      'pais':             specific_data[13].value,
      'codigo_postal':    specific_data[14].value,
      'referencias':      specific_data[15].value
    },
    'status':             "Activo"
  }
  var user = specific_data[2].value;
  //console.log(user)
  usuario.find({'id': user})
  .then((rawResponse) =>{
    //console.log(rawResponse)
    //console.log(rawResponse.length)
    //console.log(rawResponse[0].datos_personales)
    if(rawResponse.length >= 1){
      console.log('es igual')
      return res.send({message: 'Este correo ya fue registrado, use uno diferente'})
    }else{
      console.log('no esta registrado')
      usuario.create(query,function (err, nuevo_usuario){
        if(err){
          return res.status(500).send('Error en la peticion');
        }
        if(!nuevo_usuario)
          return res.status(404).send({message: 'Ningun registro identificado'});
        else
          return res.send({message: 'Saved!'});
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });

  /*usuario.create(query,function (err, nuevo_usuario){
    if(err){
      return res.status(500).send('Error en la peticion');
    }
    if(!nuevo_usuario)
      return res.status(404).send({message: 'Ningun registro identificado'});
    else
      return res.status(404).send({message: 'Saved!'});
  });*/
});

//delete user
router.post('/delete-user/:id', function(req, res, next) {
  console.log("Entro al metodo")
  var id_user = req.params.id;
  console.log(id_user)
  //metodo para buscar el usuario
  usuario.find({'id': id_user})
  .then((rawResponse) =>{
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      //metodo que cambia el stock
      usuario.updateOne({'id': id_user}, {'status': "Inactivo" })
      .then((usuario) => {
        console.log('update')
        console.log(usuario)
        var result = {'success':true}
        console.log(result)
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

//delete user
router.post('/active-user/:id', function(req, res, next) {
  console.log("Entro al metodo")
  var id_user = req.params.id;
  console.log(id_user)
  //metodo para buscar el usuario
  usuario.find({'id': id_user})
  .then((rawResponse) =>{
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      //metodo que cambia el stock
      usuario.updateOne({'id': id_user}, {'status': "Activo" })
      .then((usuario) => {
        console.log('update')
        console.log(usuario)
        var result = {'success':true}
        console.log(result)
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

module.exports = router;
