const express = require('express')
const router = express.Router()
const path = require('path')
const fs = require('fs')
const excelToJson = require('convert-excel-to-json')
// const Producto = require('../models/productos')
const Producto = require('../models/productos')
const { isAuthenticated } = require('../middlewares/authMiddleware')

/* GET all products */
// router.get('/', isAuthenticated, (req, res, next) => {
//   Producto.find({}, (err, productos) => {
//     if(err) return res.json({ success: false, message: err, data: null })
//     if(!productos.length) return res.json({ success: false, message: 'Sin registros', data: null })
//     return res.json({ success: false, message: '', data: productos })
//   })
// })
router.get('/', async (req, res) => {
  /*
    parseInt(base 10) para validar que los parametros sean numeros
  */
  const options = {
    page: parseInt(req.query.page, 10) || 1,
    limit: parseInt(req.query.limit, 10) || 10
  }
  console.log(options)
  try {
    /*
      paginate:
      primer parametro es el query para busqueda,
      segundo parametro son para options
    */
    const productos = await Producto.paginate({}, options)
    return res.json({ success: true, message: '', data: productos })
  } catch(err){
    return res.status(409).json({ success: false, message: err, data: null })
  }
  // Producto.paginate({}, (err, productos) => {
  //   if(err) return res.json({ success: false, message: err, data: null })
  //   if(!productos.length) return res.json({ success: false, message: 'Sin registros', data: null })
  //   return res.json({ success: false, message: '', data: productos })
  // })
})

router.post('/update', isAuthenticated, (req, res, next) => {
  const data = req.body
  const query = {
    $set: {
      'codigo':         data.codigo,
      'clave_interna':  data.clave_interna,
      'nombre':         data.nombre,
      'descripcion':    data.descripcion,
      'tipo':           data.tipo,
      'marca':          data.marca,
      'precio':         data.precio,
      'autos':          data.autos,
      'stock':          data.stock,
      'categoria':      data.categoria,
      'status':         data.status,
      'id_promo':       data.id_promo || '0',
      'pesado':         data.pesado,
      'outlet':         data.outlet,
      'fecha':          data.fecha,
      'usuario':        data.usuario,
      'peso':           data.peso,
      'ancho':          data.ancho,
      'alto':           data.alto,
      'largo':          data.largo
    }
  }
  // console.log(query)
  Producto.findOneAndUpdate({ '_id': data._id }, query, { new:true }, (err, product) => {
    if(!product) return res.send({ success: false, message: 'Error al actualizar', data: [] })
    if (req.files) {

      const files = req.files
      const images = []
      const dir = path.normalize(__dirname + '../../../../../replace-sys/imagenes_productos/' + data.clave_interna +'/')

      for (let i = 0; i < 3; i++) {
        images.push({
          image: files['images['+i+']'],
          path: dir + data.clave_interna + `-00${i + 1}.jpg`
        })
      }

      fs.mkdir(dir ,function(e){
        if(!e || (e && e.code === 'EEXIST')){
          //do something with contents

          images.forEach(file => {
            if (file.image) {
              const archivo = fs.writeFileSync(file.path, file.image.data)
              console.log('archivo: ',archivo)
            }
          })

          return res.json({ success:true, message: 'Datos actualizados', data: product })            
        } else {
          console.log('no existe: ',e);
          return res.json({ success:true, message: 'Datos actualizados. Error al guardar imagenes', data: product })
        }
      })
    } else {
      return res.json({ success:true, message: 'Datos actualizados', data: product })
    }
  })
})

router.post('/upload', async (req, res, send) => {
  const validFormats = [
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-excel'
  ]

  const file = req.files && req.files.file
  // Validar recepcion de archivo y formato correcto
  if (!file || !validFormats.includes(file.mimetype)) return res.status(409).send({ success: false, message: 'Error en el archivo', data: null })

  if (!excelHasValidStructure(file.data)) return res.status(409).send({ success: false, message: 'El archivo no cumple con las estructura requerida', data: null })

  /*
    Si hay valores vacios remplazar por null:
    sheetStubs: true
    **No aplica porque si hay mas filas completas sin datos pero agregadas al excel, se leeran y crearan registros con data completa como null
    **Model de product tiene datos como default: null
  */
  const excel = excelToJson({
    source: file.data, // fs.readFileSync return a Buffer
    header:{
      rows: 1 // 2, 3, 4, etc.
    },
    columnToKey: {
      A: 'codigo',
      B: 'clave_interna',
      C: 'nombre',
      D: 'equivalencias',
      E: 'categoria',
      F: 'subcategoria',
      G: 'proveedor',
      H: 'precio',
      I: 'autos',
      J: 'stock',
      K: 'servicio_pesado',
      L: 'popoye',
      M: 'imagen'
    }
  })

  // Filtar resultado para asegurar que los datos a ingresar si contengan clave_interna
  const validRows = excel[Object.keys(excel)[0]].filter(row => row.clave_interna)
  // Nuevo objeto parseando el string de autos a Array 
  const toInsert = validRows.map(row => {
    /*
      Formatear obj a insertar
      imagen: split crea array cada "," | filter para eliminar registros vacios en el array | map para quitar espacios adicionales en items de array
    */
    return {
      ...row,
      codigo: row.codigo.toString().toLowerCase(),
      clave_interna: row.clave_interna.toString().toLowerCase(),
      nombre: row.nombre.toLowerCase(),
      equivalencias: row.equivalencias ? row.equivalencias.toString().toLowerCase() : '',
      categoria: row.categoria ? row.categoria.toLowerCase() : '',
      subcategoria: row.subcategoria ? row.subcategoria.toLowerCase() : '',
      proveedor: row.proveedor ? row.proveedor.toLowerCase() : '',
      precio: (Math.round(row.precio * 100) / 100).toFixed(2),
      autos: formatAutosString(row.autos),
      imagen: row.imagen.split(',').filter(i => i.trim()).map(i => i.trim())
    }
  })

  try {
    console.log('inicio insercion/actualizacion')
    console.time('insert')
    // upsertMany retorna object con info de registros actualizados e insertados
    const upsertResponse = await Producto.upsertMany(toInsert)
    console.timeEnd('insert')
    return res.json({ success: true, message: 'Operación correcta', data: upsertResponse })
  } catch (err) {
    console.log(err.errors)
    return res.status(409).send({ success: false, message: err.errors, data: null })
  }
})

function excelHasValidStructure (file) {
  const excel = excelToJson({
    source: file, // fs.readFileSync return a Buffer
    range: 'A1:M1'
  })

  /*
    Obtener object de la primer hoja, sin importar el nombre de la hoja:
    excel[Object.keys(excel)[0]][0]
  */
  const { A, B, C, D, E, F, G, H, I, J, K, L, M } = excel[Object.keys(excel)[0]][0]
  if (
    A === 'CLAVE_EXTERNA' &&
    B === 'CLAVE_INTERNA' &&
    C === 'NOMBRE' &&
    D === 'EQUIVALENCIA' &&
    E === 'CATEGORIA' &&
    F === 'SUBCATEGORIA' &&
    G === 'PROVEEDOR' &&
    H === 'PRECIO' &&
    I === 'AUTOS' &&
    J === 'STOCK' &&
    K === 'SERVICIO_PESADO' &&
    L === 'POPOYE' &&
    M === 'IMAGEN'
  ) return true
  return false
  // console.log(excel.Hoja1)
}

function formatAutosString (autos) {
  /*
   Validar que sea un string con la estructura de autos que se requiere,
   si no cumple regresamos array vacio
  */
  if (!autos.includes('#') && !autos.includes('&')) return []

  const list = autos.split('#')

  const arr = []
  list.forEach(row => {
    const iofModelo = row.indexOf('&'),
          iofAnios = row.indexOf(','),
          iofMotor = row.indexOf('$')

    // const item = {
    //   armadora: row.substring(0, iofModelo),
    //   modelo: getModelo(row, iofModelo, iofAnios, iofMotor),
    //   anios: row.substring(iofAnios + 1, iofMotor),
    //   motor: getMotor(row, iofMotor)
    // }
    arr.push({
      armadora: row.substring(0, iofModelo),
      modelo: getModelo(row, iofModelo, iofAnios, iofMotor),
      anios: row.substring(iofAnios + 1, iofMotor),
      motor: getMotor(row, iofMotor)
    })
  })

  const arrUnique = [...new Map(arr.map(item => [
    item.armadora+item.modelo+item.anios+item.motor,
    item
  ])).values()]

  return arrUnique.map(item => {
    return {
      armadora: item.armadora ? item.armadora.toLowerCase() : '',
      modelo: item.modelo ? item.modelo.toLowerCase() : '',
      anios: item.anios,
      motor: item.motor ? item.motor.toLowerCase() : ''
    }
  })
}

function getModelo (row, inicio, final, iofMotor) {
  // Si no hay inicio no existe modelo en el string
  if (inicio < 0) return ''
  // Final es el inicio de iofAnios
  if (final < 0) {
    // No hay final, validar si hay motor y tomarlo como punto final, sino substriing hasta el final de la caden
    return iofMotor > 0
      ? row.substring(inicio + 1, iofMotor)
      : row.substring(inicio + 1, row.lenght)
  }
  // si hay iofAnios, regresar desde modelo hasta inicio de iofAnios
  return row.substring(inicio + 1, final)
}

function getMotor (row, inicio) {
  if (inicio < 0) return ''
  return row.substring(inicio + 1, row.lenght).split('%').join(', ').trim()
}

module.exports = router;
