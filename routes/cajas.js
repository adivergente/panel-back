var express = require('express');
var router = express.Router();
var cajas = require('../models/cajas');

router.get('/list-cajas', function(req, res, next) {
    cajas.find({ }, function (err, cajas){
        if(err){
            console.log('error en caja:',err)
            return res.status(500).send({success:false,message:'Error en la peticion',data:null});
        }
        if(!cajas){
            console.log('cajas',cajas)
            return res.status(404).send({success:false,message: 'Ningun registro identificado',data:null});
        }
        console.log('correcto: ',cajas)
        return res.json({success:true,message: 'Registros encontrados',data:cajas});
    });
});

router.post('/add_cajas', function(req, res, next) {
    var datos = req.body
    var query = {
        'codigo':datos.codigo,
        'alto':datos.alto,
        'largo':datos.largo,
        'ancho':datos.ancho,
    }
    cajas.create(query, function (err, cajas){
        if(err){
            console.log('error en caja:',err)
            return res.status(500).send({success:false,message:'Error en la peticion',data:null});
        }
        if(!cajas){
            console.log('cajas',cajas)
            return res.status(404).send({success:false,message: 'No se guardo registro',data:null});
        }
        console.log('correcto: ',cajas)
        return res.json({success:true,message: 'Registros guardados',data:cajas});
    });
});

router.post('/edit_cajas', function(req, res, next) {
    var datos = req.body
    var query = {
        'alto':datos.alto,
        'largo':datos.largo,
        'ancho':datos.ancho,
    }    
    cajas.updateOne(query, {'codigo': datos.codigo })
    .then((cajas) => {
        console.log('update')
        console.log(cajas)
        if(!cajas){
            console.log('cajas',cajas)
            return res.status(404).send({success:false,message: 'No se guardo registro',data:null});
        }else{
            return res.json({success:true,message: 'Registros guardados',data:cajas});
        }
    })
    .catch((err) => {
        cons
        return res.status(500).send({success:false,message:'Error en la peticion',data:null});
    });
});

router.post('/delete_caja', function(req, res, next) {
    var datos = req.body   
    console.log('borrar caja:',datos)
    cajas.remove( {'codigo': datos.codigo })
    .then((cajas) => {
        console.log('borrada')
        console.log(cajas)
        if(!cajas){
            console.log('cajas',cajas)
            return res.status(404).send({success:false,message: 'No se guardo registro',data:null});
        }else{
            return res.json({success:true,message: 'Registros guardados',data:cajas});
        }
    })
    .catch((err) => {
        console.log(err)
        return res.status(500).send({success:false,message:'Error en la peticion',data:null});
    });
});


module.exports = router;