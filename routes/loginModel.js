let sbdb = require('knex')(require('./knexfile'))

let login = {
    identifyUser: function (user, password) {
        //console.log(password)
        return sbdb.select('usuarios.CveUsario','usuarios.NombreUsuario','usuarios.emailUsuario','usuarios.accesso','usuarios.celularUsario','roles.nombre as rol','roles.accesso').from('usuarios')
            .leftJoin('roles','roles.id','usuarios.accesso')
            .where({ 'usuarios.emailUsuario': user, 'usuarios.PasswordUsuario': password })
            .then(function (result) {
                if (result.length == 1) {
                    return {
                        "success": true,
                        "data": {
                            "id": result[0].CveUsario,
                            "nickname": result[0].NombreUsuario,
                            "rol": result[0].rol,
                            //"sucursal": result[0].sucursal,
                            //"foto": result[0].foto,
                            //"name": result[0].name,
                            //"telefono": result[0].telefono,
                            "email": result[0].celularUsuario,
                            "celular": result[0].celular,
                            //"direccion": result[0].direccion,
                            //"cp": result[0].cp,
                            "acceso": result[0].accesso
                        }
                    }
                } else {
                    return { "success": false }
                }
            }).catch(function (error) {
                console.log("Login Model => identifyUser =>" + error)
                return { "success": false }
            })
    },
    forget: function (email, password) {
        return sbdb.select("*")
            .from("users")
            .where({ "email": email })
            .then(function (users) {
                if (users.length == 1) {
                    return sbdb('users')
                        .where({ "email": email })
                        .update({ "password": password })
                        .then(function (result) {
                            return {
                                "success": true,
                                "data": users[0].nickname
                            }
                        }).catch(error => {
                            return { "success": false, "error": error }
                        })
                } else {
                    return { "success": false, "error": "Usuario no existe" }
                }
            }).catch(function (error) {
                return { "success": false, "error": error }
            })
    }
}

module.exports = login