var express = require('express');
const excelToJson = require('convert-excel-to-json');
//const fileUpload = require('express-fileupload');
var router = express.Router();
var producto = require('../models/productos');
//var promocion = require('../models/promociones');
var path = require('path')
//Ruteador imagenes
//router.use(fileUpload());

//const csv = require('csv-parser');
const fs = require('fs');
const { raw } = require('body-parser');
const results = [];

/* GET all products */
router.get('/list', function(req, res, next) {
  producto.find({ }, function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    //console.log(producto)
    return res.json(producto);
  });
});

/* GET product by intern code */
router.get('/list/:code', function(req, res, next) {
  var code = req.params.code;
  producto.findOne({ clave_interna: code }, function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    //console.log(producto)
    return res.send(producto);
  });
});

/* GET product by intern code */
router.get('/activar', function(req, res, next) {
  producto.update({'status': "PENDIENTE"},{$set:{status:"ACTIVO"}},{multi:true}, function (err, producto){
      if(err)
        return res.status(500).send({success:true,message:'Error en la peticion'});
      if(!producto)
        return res.status(404).send({success:false,message: 'Ningun registro identificado'});
    //console.log(producto)
    return res.send({success:true, message: "productos modificados", data:producto});
  });
});

/* Update product */
router.post('/update-product', function(req, res, next) {
  var cont1 = 0
  var cont2 = 0
  var cont3 = 0
  let img1 = '';
  let img2 = '';
  let img3 = ''; 
  var route1 = '';
  var url = '';
  var cont1 = '';
  var url2 = ''
  var cont2 = ''
  var url3 = ''
  var cont3 = ''
  var specific_data = req.body;
  if(req.files != null){
    console.log('archivo info',req.files.length )
    //let EDFile = req.files.uploadedfile.name
    
    
    console.log('Datos de producto a actualizar')
    console.log(specific_data)
    img1 = req.files.image1;
    img2 = req.files.image2;
    img3 = req.files.image3;
    //var url = path.normalize(__dirname + '/../../Files/Destajos/')
    console.log('imagenes: ',img1)
    console.log('imagenes2: ',img2)
    console.log('imagenes3: ',img3)
    route1 = path.normalize(__dirname + "../../../../../replace-sys/imagenes_productos/" + specific_data.clave_interna +"/")
    //var url = route1 +  specific_data.clave_interna +"-001.jpg"
    if(img1 != undefined){
      url = route1 +  specific_data.clave_interna +"-001.jpg"
      cont1 = 1
      console.log(url)
    }
    if(img2 != undefined){
      url2 = route1 +  specific_data.clave_interna +"-002.jpg"
      cont2 = 1
      console.log(url2)
    }
    //var url2 = route1 +  specific_data.clave_interna +"-002.jpg"
    if(img3 != undefined){
      url3 = route1 +  specific_data.clave_interna +"-003.jpg"
      cont3 = 1
      console.log(url3)
    }
    //var url3 = route1 +  specific_data.clave_interna +"-003.jpg"
    //var url3 = route1 +  img3.name
    console.log(route1)
  
  }
  
  
  
  /*let img2 = req.files.img2;
  var route2 = "../admin/imagenes/" + specific_data[1].value + "-002.jpg"
  let img3 = req.files.img3;
  var route3 = "../admin/imagenes/" + specific_data[1].value + "-003.jpg"
*/

var query = {
  'codigo': specific_data.codigo,
  'clave_interna': specific_data.clave_interna,
  'nombre': specific_data.nombre,
  'descripcion': specific_data.descripcion,
  'tipo': specific_data.tipo,
  'marca': specific_data.marca,
  'precio': specific_data.precio,
//    'imagenes':specific_data[7].value,
  'autos': specific_data.autos,
  'stock': specific_data.stock,
  'categoria': specific_data.categoria,
  'status': 'Activo',
  'id_promo': specific_data.id_promo,
  'outlet': specific_data.outlet,
  'pesado': specific_data.pesado,
  'alto': specific_data.alto,
  'ancho': specific_data.ancho,
  'largo': specific_data.largo
}
console.log('producto: ',query)
producto.find({'clave_interna': specific_data.clave_interna})
.then((rawResponse) =>{
  if(!rawResponse){
    return res.status(404).send({message: 'Ningun registro identificado'});
  }else{
    console.log('correcto')
    //metodo que cambia el stock
    producto.updateOne({'clave_interna': specific_data.clave_interna}, query)
    .then((producto) => {
      console.log('update')
      console.log(producto.nModified)
     
        //var result = {'success':true}
        //console.log(result)
        //return res.json(result);
        if((img1 != undefined)||(img2 != undefined)||(img2 != undefined)){
          fs.mkdir(route1 ,function(e){
            if(!e || (e && e.code === 'EEXIST')){
                //do something with contents
               
                if(cont1 == 1){
                  console.log('existe1')
                  var data1 = req.files.image1.data
                  let archivo1 = fs.writeFileSync(url, data1);
                  console.log('archivo: ',archivo1)
                }
                if(cont2 == 1){
                  console.log('existe1')
                  var data2 = req.files.image2.data
                  let archivo2 = fs.writeFileSync(url2, data2);
                  console.log('archivo: ',archivo2)
                }
                if(cont3 == 1){
                  console.log('existe1')
                  var data3 = req.files.image3.data
                  let archivo3 = fs.writeFileSync(url3, data3);
                  console.log('archivo: ',archivo3)
                }
                return res.json({success:true,message:'Se guardo Correctamente',data:null});

                
            } else {
                //debug
                console.log('no existe: ',e);
                return res.json({success:true,message:'Se guardo Correctamente',data:null});
            }
          });
      
        
      }else{
        return res.json({success:false,message:'No se Modifico',data:null});
      }
      

    })
    .catch((err) => {
      console.log('error en la consulta')
      console.log(err)
      return res.json({success:false,message:'No se Modifico',data:null});
    });
  }
})
.catch((err) => {
  console.log(err)
  return res.json({success:false,message:'No se Encontro',data:null});
});
  
  /*producto.create(query)
  .then((producto) =>
  {*/
        /*console.log('correcto')
        img1.mv(route1)
        .then((producto) =>
        {
          console.log('imagen 2 correcto')
          img2.mv(route2)
          .then((producto) =>
            {
              console.log('imagen 3 correcto')
              img3.mv(route3, function(err)
              {
                if (err)
                  return res.status(500).send(err);
                res.send('File uploaded!');
              })
              .catch((err) =>
              {
                return res.status(500).send('Error en la peticion');
              });
            });
        });*/
  //});
  //let EDFile = req.files
  //const name = parametros.myfile.name
  //console.log('datos: ',datos)
  //const data = req.files.uploadedfile.data
  //const data = req.files
  //console.log('nombre: ',EDFile)
  //console.log('archivos: ',data)
  
  

  /*var query = {
    'codigo': specific_data[0].value,
    'clave_interna': specific_data[1].value,
    'nombre': specific_data[2].value,
    'descripcion': specific_data[3].value,
    'tipo': specific_data[5].value,
    'marca': specific_data[6].value,
    'precio': specific_data[8].value,
//    'imagenes':specific_data[7].value,
    'autos': specific_data[7].value,
    'stock': specific_data[9].value,
    'categoria': specific_data[4].value,
    'status': specific_data[11].value,
    'id_promo': specific_data[10].value
  }*/
  //console.log('datos en query')
  //console.log(query)
  //metodo para buscar el producto
  /*producto.find({'clave_interna': specific_data[1].value})
  .then((rawResponse) =>{
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      //metodo que cambia el stock
      producto.updateOne({'clave_interna': specific_data[1].value}, query)
      .then((producto) => {
        console.log('update')
        console.log(producto)
        var result = {'success':true}
        console.log(result)
        return res.json(result);

      })
      .catch((err) => {
        console.log('error en la consulta')
        console.log(err)
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });*/
});


router.post('/newFile', function(req, res, next) {
  console.log('archivo info')
  let EDFile = req.files.uploadedfile.name
  //const name = parametros.myfile.name
  //console.log('datos: ',datos)
  const data = req.files.uploadedfile.data
  console.log('nombre: ',EDFile)
  console.log('archivos: ',data)
  var usuario = data.body.usuario
  const result = excelToJson({
    source: data, // fs.readFileSync return a Buffer
    header:{
      rows: 1 // 2, 3, 4, etc.
    },
    columnToKey: {
      A: 'CLAVE_EXTERNA',
      B: 'CLAVE_INTERNA',
      C: 'NOMBRE',
      D: 'DESCRIPCION',
      E: 'CATEGORIA',
      F: 'SUBCATEGORIA',
      G: 'PROVEEDOR',
      H: 'PRECIO',
      I: 'AUTOS',
      J: 'STOCK',
      K: 'SERVICIO_PESADO',
      L: 'OUTLET'}
    });
    var f = new Date();
  console.log('data: ',result.Productos.length)
    for(var i in result.Productos){
      console.log('entro')
      result.Productos[i].id_promo = "";
      console.log(result.Productos[i].id_promo)
      result.Productos[i].status = "PENDIENTE";
      result.Productos[i].status = usuario;
      result.Productos[i].fecha = new Date(f.getFullYear(),f.getMonth() +1,f.getDate(),f.getHours(),f.getMinutes(),f.getSeconds()).toISOString()
    }
    //console.log('data: ',result.Productos)
  //console.log('data: ',result[0])
  var resultado = {'name':EDFile,'result1':result}
  //console.log('resultado del excel: ',resultado)
  var query = {}
  var num = 0
  for(var i in result.Productos){
     query = {
      'codigo': result.Productos[i].CLAVE_EXTERNA,
      'clave_interna': result.Productos[i].CLAVE_INTERNA,
      'nombre': result.Productos[i].NOMBRE,
      'descripcion': result.Productos[i].DESCRIPCION,
      'tipo': result.Productos[i].SUBCATEGORIA,
      'marca': result.Productos[i].PROVEEDOR,
      'precio': result.Productos[i].PRECIO,
  //    'imagenes': [ specific_data[10].value, specific_data[11].value, specific_data[12].value ],
      'autos': result.Productos[i].AUTOS,
      'stock': result.Productos[i].STOCK,
      'categoria': result.Productos[i].CATEGORIA,
      'status': result.Productos[i].status,
      'id_promo': result.Productos[i].id_promo,
      'pesado': result.Productos[i].SERVICIO_PESADO,
      'outlet': result.Productos[i].OUTLET,
      'fecha':result.Productos[i].fecha
    }
    //console.log('pendiente: ',query)
    producto.create(query)
    .then((producto) =>
    {
      if(!producto){
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        //console.log('correcto: ',i)
        num++
        if(!producto){
          return res.status(404).send({message: 'Ningun registro identificado'});
        }else{
          console.log('correcto: ',num)
          //num++
          //return res.send({success:true,message:'File uploaded!',data:resultado});
          //metodo que cambia el stock
          console.log('num: ',num)
          if(num == result.Productos.length){
            console.log('correcto')
            return res.send({success:true,message:'File uploaded!',data:resultado});
          }else{
            console.log('fallo')
            //return res.send({success:false,message:'No se subio',data:resultado});
          }
        } 
        //return res.send({success:true,message:'File uploaded!',data:resultado});
        //metodo que cambia el stock
        
      }
     })
    .catch((err) => {
      res.send({success:true,message:'File uploaded!',data:resultado});
    });
  }
  /*
  if(!producto){
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        console.log('correcto: ',num)
        num++
        //return res.send({success:true,message:'File uploaded!',data:resultado});
        //metodo que cambia el stock
        console.log('num: ',num)
        if(num == result.Productos.length){
          console.log('correcto')
          return res.send({success:true,message:'File uploaded!',data:resultado});
        }else{
          console.log('fallo')
          //return res.send({success:false,message:'No se subio',data:resultado});
        }
      } */
      //console.log(num)
  //return res.send({success:true,message:'File uploaded!',data:resultado});  
});
//create new producto
router.post('/new-product', function(req, res, next) {
  var specific_data = req.body;
  console.log('Datos de nuevo producto')
  console.log(specific_data)
  var f = new Date();
  var query = {
    'codigo': specific_data[0].value,
    'clave_interna': specific_data[1].value,
    'nombre': specific_data[2].value,
    'descripcion': specific_data[3].value,
    'tipo': specific_data[5].value,
    'marca': specific_data[6].value,
    'precio': specific_data[8].value,
//    'imagenes': [ specific_data[10].value, specific_data[11].value, specific_data[12].value ],
    'autos': specific_data[7].value,
    'stock': specific_data[9].value,
    'categoria': specific_data[4].value,
    'status': "Activo",
    'id_promo': "0",
    'fecha':new Date(f.getFullYear(),f.getMonth() +1,f.getDate(),f.getHours(),f.getMinutes(),f.getSeconds()).toISOString(),
    'usuario':specific_data[10].value,
    'peso':specific_data[11].peso,
    'alto': specific_data[12].alto,
    'ancho': specific_data[13].ancho,
    'largo': specific_data[14].largo
  }

  let img1 = req.files.img1;
  var route1 = "../admin/imagenes/" + specific_data[1].value + "-001.jpg"
  let img2 = req.files.img2;
  var route2 = "../admin/imagenes/" + specific_data[1].value + "-002.jpg"
  let img3 = req.files.img3;
  var route3 = "../admin/imagenes/" + specific_data[1].value + "-003.jpg"
/*
  producto.create(query,function (err, producto){
    console.log(query)
    console.log(producto)
    if(err)
    {
      console.log("Error en la peticion")
      return res.status(500).send('Error en la peticion');
    }
    if(!producto)
    {
      console.log("ningun producto")
      return res.status(404).send({message: 'Ningun registro identificado'});
    }
    else
    {
      console.log("exito!")
      return res.status(404).send({message: 'Saved!'});
    }
  });
  */

  producto.create(query)
  .then((producto) =>
  {
        console.log('correcto')
        img1.mv(route1)
        .then((producto) =>
        {
          console.log('imagen 2 correcto')
          img2.mv(route2)
          .then((producto) =>
            {
              console.log('imagen 3 correcto')
              img3.mv(route3, function(err)
              {
                if (err)
                  return res.status(500).send(err);
                res.send('File uploaded!');
              })
              .catch((err) =>
              {
                return res.status(500).send('Error en la peticion');
              });
            });
        });
  });
});

//delete product
router.post('/delete-product/:code', function(req, res, next) {
  var code = req.params.code;
  console.log(code)
  //metodo para buscar el producto
  producto.find({'clave_interna': code})
  .then((rawResponse) =>{
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      //metodo que cambia el stock
      producto.updateOne({'clave_interna': code}, {'status': "Inactivo" })
      .then((producto) => {
        console.log('update')
        console.log(producto)
        var result = {'success':true}
        console.log(result)
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});



router.post('/activar_prom', function(req, res, next) {
  var code = req.body.id_promo;
  console.log(code)
  //metodo para buscar el producto
  var query = {
      $or : [{id_promo:""},{id_promo:{$ne:""}}]
  }
  producto.update(query,{id_promo:code},{multi:true})
  .then((rawResponse) =>{
    console.log(rawResponse)
    if(!rawResponse){
      return res.status(404).send({success:false, message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      return res.send({success:true, message: 'Datos Guardados'});
      //metodo que cambia el stock
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send({success:false, message:'Error en la peticion'});
  });
});

router.post('/activar_prom2', function(req, res, next) {
  var code = req.body;
  console.log(code.id_promo)
  console.log(code.productos.length)

  //metodo para buscar el producto
  var query = {
      
  }
  for(var i in code.productos){
    query = {_id:code.productos[i]._id}
    console.log(query)
    producto.updateOne(query,{id_promo:code.id_promo})
    .then((rawResponse) =>{
      console.log(rawResponse)
      if(!rawResponse){
        console.log({success:false, message: 'Ningun registro identificado'});
      }else{
        //console.log('correcto')
        console.log({success:true, message: 'Datos Guardados'});
        //metodo que cambia el stock
      }
    })
    .catch((err) => {
      console.log(err)
      //return res.status(500).send({success:false, message:'Error en la peticion'});
    });
  }
  return res.json({success:true, message: 'Datos Guardados'});
});

//delete product
router.post('/active-product/:code', function(req, res, next) {
  var code = req.params.code;
  console.log(code)
  //metodo para buscar el producto
  producto.find({'clave_interna': code})
  .then((rawResponse) =>{
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      //metodo que cambia el stock
      producto.updateOne({'clave_interna': code}, {'status': "Activo" })
      .then((producto) => {
        console.log('update')
        console.log(producto)
        var result = {'success':true}
        console.log(result)
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});
//productos mas vendidos

module.exports = router;
