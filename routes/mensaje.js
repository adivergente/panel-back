var express = require('express');
var router = express.Router();
var mensaje = require('../models/mensaje');

router.get('/list-mensaje', function(req, res, next) {
  mensaje.find({ }, function (err, mensaje){
        if(err){
            return res.status(500).send('Error en la peticion');
        }
        if(!mensaje){
            console.log(mensaje)
            return res.status(404).send({message: 'Ningun registro identificado'});
        }
        return res.json(mensaje);
    });
});


module.exports = router;