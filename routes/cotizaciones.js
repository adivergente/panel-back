const express = require('express')
const router = express.Router()
const mongoose =  require('mongoose')
const Cotizacion = require('../models/cotizacion')
// const Usuario = require('../models/usuarios') // en caso de envio de email
const { isAuthenticated } = require('../middlewares/authMiddleware')
// const dayjs = require('dayjs')


router.get('/', isAuthenticated, (req, res, next) => {
  Cotizacion.aggregate([
			{
				$lookup:{
	        from: "usuarios",
	        localField:"atiende",
	        foreignField:"_id", 
	        as: "user_atiende"
	      }
			},
			{ $sort: { created_at: -1 } }
  	], (err, cotizaciones) => {
  		if (err) return res.send({ success: false, message: err, data: null })
  		return res.send({ success: true, message: '', data: cotizaciones })
  	})
})

// Cambiar status de orden
router.post('/change-status/:id', isAuthenticated, (req, res, next) => {
  const query = {
    $set: {
      'status': req.body.status,
      'atiende': req.userId
    }
  }
  console.log(query)
  Cotizacion.findOneAndUpdate({ '_id': req.params.id }, query, (err, cotizacion) => {
    if(!cotizacion) return res.status(404).send({ success: false, message: 'Error al actualizar', data: [] })
    return res.json({ success:true, message: 'Actualizada status de cotización', data: null })
  })
})


module.exports = router
