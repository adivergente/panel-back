const express = require('express')
const router = express.Router()
const FormaPago = require('../models/forma_pago')
const { isAuthenticated } = require('../middlewares/authMiddleware')

router.get('/', isAuthenticated, (req, res, next) => {
	FormaPago.find({}, (err, fPago) => {
		if (err || !fPago.length) return res.status(404).send({ success: false, message: 'Ningun registro identificado' })
    return res.json({ success: true, message: '', data: fPago })
	})
})

router.post('/update', isAuthenticated, (req, res, next) => {
	const { ref, banco, nombre } = req.body
	const query = {
		$set: { ref, banco, nombre }
	}

	FormaPago.findByIdAndUpdate(req.body._id, query, { new: true }, (err, fPago) => {
		if (err || !fPago) res.send({ success: false, message: 'Error al actualizar', data: [] })
		return res.json({ success:true, message:'Datos actualizados', data: fPago })
	})
})

module.exports = router;
