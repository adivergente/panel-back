var express = require('express');
var router = express.Router();
var devoluciones = require('../models/devoluciones');

router.get('/list-dev', function(req, res, next) {
    devoluciones.find({ }, function (err, devoluciones){
        if(err){
            return res.status(500).send('Error en la peticion');
        }
        if(!devoluciones){
            console.log(devoluciones)
            return res.status(404).send({message: 'Ningun registro identificado'});
        }
        return res.json(devoluciones);
    });
});

router.post('/por-fecha-d', function(req, res, next) {
    console.log('entro a devoluciones por fechas')
    var datos = req.body
    var start = datos.fecha
    var end = datos.fecha2
    console.log(start)
    console.log(end)
    devoluciones.aggregate([
        {
            $match: {
                $and: [ 
                    {"tipo": {$eq:"D"}},
                    {"fecha_solicitud": {$lte: end} },
                    {"fecha_solicitud": {$gte: start}}
                ]
            }
        },
        {$group : 
            { 
                _id : "$fecha_solicitud",
                fecha: { $sum : "$fecha_solicitud"},
                cant : 
                    { $sum : 1 }
                
            }
        } 
            /*$and:[{
                fecha: {$gt: start}, 
                fecha: {$lt: end}
            }]*/
        ], function (err, devoluciones){
            if(err){
                console.log('error: ',err)
                return res.status(500).json({success:false,message:'Error en Consulta',data:null});
            }
            if(!devoluciones){
                console.log(devoluciones)
                return res.status(404).json({success:false,message:'No se Encontro',data:null});
            }
            console.log('respuesta: ',devoluciones)
            return res.json({success:true,message:'Correcto',data:devoluciones});
        });
        /*

         db.devoluciones.find({
                $and:[{
                    fecha: {$gt: '2020-04-01'}, 
                    fecha: {$lt: '2020-04-30'}
                }]
        })
        db.devoluciones.insert({
            "id": "1",
            "fecha_solicitud":"2020-04-23",
            "fecha_atiende":"2020-04-23",
            "fecha_devolucion":"2020-04-23",
            "tipo":"G",
            "id_compra":"1",
            "nombre_cliente":"Ernesto Esteban Ascención Treviño",
            "atiende":"",
            "descricpcion":"El cable venía roto",
            "status":"Pendiente",
            "no_guia":"",
            "pago":""
        })
         */
});

router.post('/por-fecha-g', function(req, res, next) {
    console.log('entro a devoluciones por fechas')
    var datos = req.body
    var start = datos.fecha
    var end = datos.fecha2
    console.log(start)
    console.log(end)
    devoluciones.aggregate([
        {  
            $match: {
                $and: [ 
                    {"tipo": {$eq:"G"}},
                    {"fecha_solicitud": {$lte: end} },
                    {"fecha_solicitud": {$gte: start}}
                ]
            }
        },
        {$group : 
            { 
                _id : "0",
                fecha: { $push : "$fecha_solicitud" },
                cant : 
                    { $sum : 1 }
                
            }
        } 
            /*$and:[{
                fecha: {$gt: start}, 
                fecha: {$lt: end}
            }]*/
        ], function (err, devoluciones){
            console.log('error: ',err)
            console.log('devoluciones: ',devoluciones)
            if(err){
                console.log('error: ',err)
                return res.status(500).json({success:false,message:'Error en Consulta',data:null});
            }
            if(!devoluciones){
                console.log(devoluciones)
                return res.status(404).json({success:false,message:'No se Encontro',data:null});
            }
            console.log('respuesta: ',devoluciones)
            return res.json({success:true,message:'Correcto',data:devoluciones});
        });
        /*

         db.devoluciones.aggregate(
             {  
                $match: {
                    $and: [ 
                        {"tipo": {$eq:"G"}},
                        {"fecha_solicitud": {$lte: "2020-04-30"} },
                        {"fecha_solicitud": {$gte: "2020-04-01"}}
                    ]
                }
            },
            {$group : 
                { 
                    _id : "0",
                    fecha: { $push : "$fecha_solicitud" },
                    cant : 
                        { $sum : 1 }
                    
                }
            } 
         )
        db.devoluciones.insert({
            "id": "1",
            "fecha_solicitud":"2020-04-23",
            "fecha_atiende":"2020-04-23",
            "fecha_devolucion":"2020-04-23",
            "tipo":"G",
            "id_compra":"1",
            "nombre_cliente":"Ernesto Esteban Ascención Treviño",
            "atiende":"",
            "descricpcion":"El cable venía roto",
            "status":"Pendiente",
            "no_guia":"",
            "pago":""
        })
         */
});

module.exports = router;