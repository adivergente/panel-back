const express = require('express')
const router = express.Router()
const Promocion = require('../models/promociones')
const Producto = require('../models/productos')
const path = require('path')
const fs = require('fs')
const dayjs = require('dayjs')
const { isAuthenticated } = require('../middlewares/authMiddleware')

/* GET all promociones */
router.get('/', isAuthenticated, (req, res, next) => {
  Promocion.find({ }, function (err, promociones){
    if(err || !promociones) return res.status(500).send('Error en la peticion')
    return res.send({ success: true, message: '', data: promociones })
  }) 
})

/* Ruta para prevenir bugs de modulos anteriores que dependen de este recurso */
router.get('/all', isAuthenticated, (req, res, next) => {
  Promocion.find({ }, function (err, promociones){
    if(err || !promociones) return res.status(500).send('Error en la peticion')
    return res.json(promociones)
  }) 
})

/* Create promocion */
router.post('/', async (req, res, next) => {
  const image = req.files.image
  const initDate = dayjs(req.body.fecha_in).format('YYYY-MM-DD HH:mm:ss')
  const endDate = dayjs(req.body.fecha_fin).format('YYYY-MM-DD HH:mm:ss')
  const productos = JSON.parse(req.body.productos)
  const imagePath = path.normalize(__dirname + `../../../../../replace-sys/imagenes_promociones/${image.name}`)
  const data = {
    ...req.body,
    productos,
    fecha_in: initDate,
    fecha_fin: endDate,
    ruta: image.name
  }
  const promo = new Promocion(data)

  console.log('antes de validar')
  const promoIdExist = await Promocion.find({ 'id_promo': req.body.id_promo }).then(r => r.length)
  if (promoIdExist) return res.send({ success: false, message: 'ID de la promoción ya registrado, ingrese uno diferente', data: null})
  try {
    // Metodo save() retorna objeto guardado, on erro undefined
    if (await promo.save()) {
      fs.writeFileSync(imagePath, image.data)
      // Update retorna { n: <int>, nModified: <int>, ok: <Boolean> }
      let productsModified = 0
      if (productos.length) productsModified = await Producto.updateMany({ _id: { $in: productos } }, { $set: { id_promo: req.body.id_promo } }).then(r => r.nModified)
      return res.send({ success:true, message: `Promocion guardada. ${productsModified} productos actualizados.`, data: null})
    } else {
      return res.send({ success: false, message: 'Error al guardar la promocion', data: null })
    }
  } catch (err) {
    return res.send({ success: false, message: err, data: null })
  }
})

/* Update de promociones */
router.post('/update', isAuthenticated, async (req, res, next) => {
  const image = req.files && req.files.image
  const initDate = dayjs(req.body.fecha_in).format('YYYY-MM-DD HH:mm:ss')
  const endDate = dayjs(req.body.fecha_fin).format('YYYY-MM-DD HH:mm:ss')
  const productos = JSON.parse(req.body.productos)
  let data = {
    ...req.body,
    productos,
    fecha_in: initDate,
    fecha_fin: endDate
  }
  console.log('fecha inicial', initDate)
  try {

    const promoFound = await Promocion.findById(req.body._id)
    if (promoFound) {
      const oldProducts = [...promoFound.productos]
      const toRemove = oldProducts.filter(p => !productos.includes(p))
      const toAdd = productos.filter(p => !oldProducts.includes(p))
      // console.log('productos anteriores', oldProducts)
      // console.log('productos por agregar', toAdd)
      // console.log('productos a quitar', toRemove)
      // console.log('nuevo array de productos', productos)

      let msgAdded = '0'
      let msgRemoved = '0'

      // Si existe imagen se modifica archivo
      if (image) {
        const imagePath = path.normalize(__dirname + `../../../../../replace-sys/imagenes_promociones/${image.name}`)
        fs.writeFileSync(imagePath, image.data)
      } 

      // Realizar modificaciones en productos solo si hay que agregar o quitar alguno
      if (toAdd.length || toRemove.length) {
        console.log('modificar')
        if (toAdd.length) msgAdded = await Producto.updateMany({ _id: { $in: toAdd } }, { $set: { id_promo: req.body.id_promo } }).then(r => r.nModified)

        if (toRemove.length) msgRemoved = await Producto.updateMany({ _id: { $in: toRemove } }, { $set: { id_promo: '0' } }).then(r => r.nModified)

        data = {
          ...data,
          productos
        }
      }

      await Promocion.findByIdAndUpdate(req.body._id, { $set: data })

      return res.send({ success:true, message: `Promocion actualizada. (${msgAdded} nuevos productos | ${msgRemoved} productos retirados)`, data: null})

    } else {
      return res.send({ success: false, message: 'Error: No se encontro la promoción', data: null })
    }

  } catch (err) {
    console.log(err)
    return res.send({ success: false, message: err, data: null })
  }

  // console.log(image)
  // return res.send({ success: false, message: '', data: null })
})

/* Delete promoción */
router.post('/remove', isAuthenticated, async (req, res, next) => {
  let msgRemoved = '0'

  try {

    const promoDeleted = await Promocion.findByIdAndRemove(req.body._id)

    if (promoDeleted) {

      // Eliminar imagen
      const image = path.normalize(__dirname + `../../../../../replace-sys/imagenes_promociones/${promoDeleted.ruta}`)
      /**
      * unlinkSync: Version syncrona (espera hasta que elimine para continaur)
      * https://flaviocopes.com/how-to-remove-file-node/
      */
      fs.unlinkSync(image)

      if (promoDeleted.productos && promoDeleted.productos.length) {
        msgRemoved = await Producto.updateMany(
          {_id: { $in: promoDeleted.productos } },
          { $set: { id_promo: '0' } }
        ).then(r => r.nModified)
      }


      return res.send({ success: true, message: `Eliminado ${promoDeleted.nombre} (${promoDeleted.id_promo}) | ${msgRemoved} productos modificados)`, data: null })
    }

    return res.send({ success: false, message: 'Fallo al eliminar', data: null })

  } catch (err) {
    console.log(err)
    return res.send({ success: false, message: err, data: null })
  }
})

/* GET promocion by code */
router.get('/search/:code', function(req, res, next) {
  var code = req.params.code;
  Promocion.findOne({ codigo: code }, function (err, promocion){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!promocion)
        return res.status(404).send({message: 'Ningun registro identificado'});
    //console.log(promocion)
    return res.send(promocion);
  });
});

//delete promocion
router.post('/delete-promocion/:code', function(req, res, next) {
  var code = req.params.code;
  console.log(code)
  //metodo para buscar la promocion
  Producto.find({'id_promo': code})
  .then((rawResponse) =>{
    console.log('Respuesta: ',rawResponse.length)
    if(rawResponse.length == 0){

            Promocion.find({'id_promo': code})
            .then((rawResponse) =>{
              if(!rawResponse){
                return res.status(404).send({message: 'Ningun registro identificado'});
              }else{
                console.log('correcto')
                //metodo que cambia el stock
                Promocion.updateOne({'id_promo': code}, {'status': "Inactivo" })
                .then((promocion) => {
                    console.log('update')
                    console.log(promocion)

                    console.log('correcto')
                    var result = {success:true,message:'Se pudo desactivar la promocion'}
                    console.log(result)
                    return res.json(result);
                  })
                .catch((err) => {
                  console.log('err:',err)
                  var result = {success:false,message:'La promocion no se pudo modificar'}
                  return res.status(500).send(result);
                });
              }
            })
            .catch((err) => {
              console.log(err)
              var result = {success:false,message:'Error al consultar'}
              return res.status(500).send(result);
            });

    }else{
      //tipo:"HDD"},{$set:{cantidad:10}},{multi:true}
      Producto.update({'id_promo': code},{$set:{id_promo:"0"}},{multi:true})
      .then((producto) => {
          console.log('update')
          console.log(producto)

          console.log('correcto')
          var result = {success:true,message:'Se pudo desactivar la promocion'}
          console.log(result)
          Promocion.updateOne({'id_promo': code}, {'status': "Inactivo" })
          .then((promocion) => {
              console.log('update')
              console.log(promocion)

              console.log('correcto')
              var result = {success:true,message:'Se pudo desactivar la promocion'}
              console.log(result)
              return res.json(result);
            })
          .catch((err) => {
            console.log('err:',err)
            var result = {success:false,message:'La promocion no se pudo modificar'}
            return res.status(500).send(result);
          });
        })
      .catch((err) => {
        console.log(err)
        console.log('No puede eliminar')
        var result = {'success':false,message:'No se puede eliminar Tiene productos asignados'}
        console.log(result)
      });
      
      //return res.json(result);
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

//active promocion
router.post('/active-promocion/:code', function(req, res, next) {
  var code = req.params.code;
  console.log(code)
  //metodo para buscar la promocion
  Producto.find({'id_promo': code})
  .then((rawResponse) =>{
    if(rawResponse.length==0){

            Promocion.find({'id_promo': code})
            .then((rawResponse) =>{
              if(!rawResponse){
                return res.status(404).send({message: 'Ningun registro identificado'});
              }else{
                console.log('correcto')
                //metodo que cambia el stock
                Promocion.updateOne({'id_promo': code}, {'status': "Activo" })
                .then((promocion) => {
                    console.log('update')
                    console.log(promocion)

                    console.log('correcto')
                    var result = {success:true,message:'Se pudo activar la promocion'}
                    console.log(result)
                    return res.json(result);
                  })
                .catch((err) => {
                  return res.status(500).send('Error en la peticion');
                });
              }
            })
            .catch((err) => {
              console.log('err:',err)
                  var result = {success:false,message:'La promocion no se pudo activar'}
                  return res.status(500).send(result);
            });

    }else{
      console.log('No puede activar')
      var result = {'success':false}
      console.log(result)
      return res.json(result);
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

router.post('/update-promocion', function(req, res, next) {
  
  var datos = req.body;
  console.log("datos promocion: ",datos)
  var id_promo = datos.id_promo
  var query = {
    //'ruta': "/Users/macbookair/Documents/refacesys/replace-sys/admin-backend/app/public/imagenes_promociones/" + req.files.img1.name,
    'id_promo': datos.id_promo,
    'nombre': datos.nombre,
    'descripcion': datos.descripcion,
    'descuento': datos.descuento,
    'status': datos.status
  }
  console.log("query:", query)
  //metodo para buscar el producto
  Promocion.find({'id_promo': datos.id_promo})
  .then((rawResponse) =>{
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      //console.log(rawResponse)
      //var result = {'success':true}
      //return res.json(result);
      //metodo que cambia el stock
      Promocion.updateOne({'id_promo': id_promo}, query)
      .then((promocion) => {
        console.log('update')
        console.log(promocion)
        var result = {'success':true}
        console.log(result)
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});


module.exports = router;
